# bacui

bacui provides useful and basic package management commands in a convenient and easy to use text interface for Bedrock Linux.

bacui is aimed at experienced/intermediate/advanced users of Bedrock Linux, who have at least basic knowledge of their Linux system.

`Attention: bacui is still at early development satags and can break packages on your system or even your entire system`

## About
This is a fork of [pacui](https://github.com/excalibur1234/pacui) that follows the KISS principle: The whole script is contained within one file, which consists of easy to read bash code with many helpful comments.
bacui is based upon pacui and modified to suit most distro needs, therefore the changes in feature implementations or decreas in functionality. (some will be reimpemented in the [future](#todo))

This aims to provide a single UI for bedrock users that will work across multiple stratum, however this is not a packamanger, it is just a shell to fetch different package manager commands for end users.

## Usability

bacui byfar support these distros

| Distro |  |
|---|---|
| alpine | support |
| arch | support |
| centos | support |
| Debian | tested |
| Devuan | support |
| fedora | tested |
| gentoo | tested |
| void | tested |
| void-musl | tested |
| manjaro| tested |
| ubuntu | tested |

bacui only works with stratum with its default naming, therefore renamed stratum and derivatives distros will not work (a little hack is to rename the stratum to it's base distro)

Table of Contents
-----------------

   * [Screenshot](#screenshot)
   * [Installation](#installation)
      * [Dependencies](#dependencies)
      * [Normal instalation](#normal-installation)
      * [Execute without prior Installation](#execute-without-prior-installation)
   * [Usage](#usage)
      * [Start bacui with UI](#start-bacui-with-ui)
      * [Start bacui without UI: Using Options](#start-bacui-without-ui-using-options)
      * [Start bacui without UI: Using Options and Package Names](#start-bacui-without-ui-using-options-and-package-names)
      * [Start bacui without UI: Passing Arguments to AUR helper or Pacman](#start-bacui-without-ui-passing-arguments-to-aur-helper-or-pacman)
      * [Multiple installed AUR helpers](#multiple-installed-aur-helpers)
   * [Help](#help)
   * [Todo](#todo)


## Screenshot

UI of bacui:

![Screenshot 01](https://i.imgur.com/9jYcL1I.png)

## Installation

### Dependencies

please install its dependencies [fzf](https://github.com/junegunn/fzf)

### Normal installation
Install by executing [installer.sh](https://gitlab.com/timescam/bacui/raw/master/bacuinstaller.sh?inline=false), and it will download the lastest code from gitlab.
this is not essential, as it simply but bacui in bin and make it executable, which can be manually done.

### Execute without prior Installation
For a minimal working version of PacUI, please install its dependencies and [fzf](https://github.com/junegunn/fzf) using Pacman first (if possible). Then, the PacUI file can be downloaded and run without prior installation:
```
wget https://raw.githubusercontent.com/excalibur1234/pacui/master/pacui
```
```
bash pacui
```
I find this feature of PacUI invaluable for fixing systems. Here are two examples:
- A large number of updates broke (parts of) the GUI, e.g. xorg, window manager, or desktop environment. In this case, switching to a different tty (with CTRL + ALT + F2), installing PacUI and using "Roll Back System" to roll back all the latest updates can fix the system (temporarily).
- A broken keyring makes it impossible to apply updates or install any packages. Executing PacUI without prior installation and using "Fix Pacman Errors" (which does not require "expac" or "fzf") to fix the keyring and all related problems is the easiest and fastest solution I know of.


## Usage

### Start bacui with UI
After successful installation, type the following command into your terminal in order to start bacui with a simple UI:
```
bacui
```

### Start bacui without UI: Using Options
Using bacui without its UI requires less keystrokes and can therefore be much quicker. An overview of all bacui options is displayed with `bacui h`.

For example, you want to display the **U**pdate System. Please note the marked letter "U"  in bacui's corresponding UI option.
bacui does not care, whether you use upper or lower case letters as options or whether you use no, one or two dashes in front. Therefore, the following four commands are equivalent:
- `bacui U`
- `bacui u`
- `bacui -u`
- `bacui --u`

This principle can be used with all of bacui's options. 

### Start bacui without UI: Using Options and Package Names
You can also use package names in addition to options. For example, you want to install the package "cantata". Then, you can use a command like
```
bacui i cantata
```
Instead of a list of all available packages, a much shorter already filtered list is displayed. Simply select the "cantata" package you want to install and press ENTER in order to install it.

If an argument contains special characters, it has to be quoted. For example when using regular expressions in order to search package file names starting with "zsh":
```
bacui s "^zsh"
```

### Start bacui without UI: Passing Arguments to AUR helper or Pacman
For advanced use (e.g. in scripting or an alias), bacui can have a "flag" argument, which gets passed directly to an AUR helper and/or Pacman.
Examples:
- ` bacui -r 0ad --flag="--noconfirm"`
- ` bacui u flag --noconfirm`


### Multiple installed AUR helpers
Please note that bacui optionally requires at least one of these AUR helpers to enable use of the AUR.: [Yay](https://github.com/Jguer/yay), [Pikaur](https://github.com/actionless/pikaur), [Aurman](https://github.com/polygamma/aurman), [Pakku](https://github.com/kitsunyan/pakku), [Trizen](https://github.com/trizen/trizen), or [Pacaur](https://github.com/rmarquis/pacaur).

If more than one AUR helper is installed, they are automatically used in the same order as listed above (i.e. Aurman is used with priority while Pacaur is only used as a last resort). A specific AUR helper can be set with the `bacui_AUR_HELPER` environment variable.

Alternatively, replacing `$bacui_AUR_HELPER` with the name of your preferred AUR helper in the following variable (within `/usr/bin/bacui` file) works as well. Note that `$bacui_AUR_HELPER` needs to be replaced (again) after each bacui update:
```
# here, the preferred AUR helper can be set manually by the user. for example, AUR_Helper="trizen" uses Trizen despite any other installed AUR helpers.
AUR_Helper="$bacui_AUR_HELPER"
```
worth noting that both arch and manajro have aur related settings with _A and _M at the variable end.

## Help

Choose the "Help" option within bacui's UI by entering "00" or "H" or "h" or "help" and pressing "ENTER".
When using bacui without UI, use one of the following commands:
- `bacui h`
- `bacui -h`
`bacui --help` from the terminal will call bacui's help page, too.

## Todo
| tasks | diffculties | pirority |
|---|---|---|
| bug fixs | ? | highest |

features that are on hold
*  renamed strata support (would require the strat dedection to be rewritten, currently no plans for this )
*  most devlopement are on hold as pmn is comming soon


`Please note that I am not affiliated with the development of Bedrock Linux, and that I am not responsible for any data loss or damaged device`

Any Bedrock Linux related issues shall also not be flaged here instead to [bedrocklinux-userland](https://github.com/bedrocklinux/bedrocklinux-userland)