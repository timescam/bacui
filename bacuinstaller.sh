echo
echo -e " \e[41m bacui installer \e[0m"
echo
echo -e " this script will now install bacui on your system"
echo
echo -e "\e[1;31m Attention: bacui is still at early development satages and can break packages on your system or even your entire system. \e[0m"
echo
echo -e " \e[41m Do you wish to proceed? [Y|N] \e[0m"
echo
                # save answer in "answer" variable:
                read -r -n 1 -e answer

                case ${answer:-n} in                                                # if ENTER is pressed, the variable "answer" is empty. if "answer" is empty, its default value is "n".

                    y|Y|yes|YES|Yes )                                               # do this, if "answer" is y or Y or yes or YES or Yes

                        cd /tmp
                        git clone https://gitlab.com/timescam/bacui.git
                        cd bacui
                        sudo cp bacui /usr/bin/
                        cp bacuinstaller.sh $HOME
                        cd /usr/bin/
                        sudo chmod +x bacui
                        cd /tmp
                        rm -rf bacui
                        echo
                        echo -e " \e[1m bacui has been installed. \e[0m"
                        echo
                        ;;


                    * )                                                             # do this in all other cases
                        echo
                        echo -e " \e[1m Installton of bacui has been cancelled. \e[0m"
                        echo
                        ;;

                esac
